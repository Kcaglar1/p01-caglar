//
//  ViewController.h
//  Hello, World!
//
//  Created by Kaan on 1/22/17.
//  Copyright © 2017 Kaan. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface ViewController : UIViewController

@property (weak,nonatomic) IBOutlet UILabel *label;
@property (weak,nonatomic) IBOutlet UIButton *button;

-(IBAction)button:(id)sender;



@end

