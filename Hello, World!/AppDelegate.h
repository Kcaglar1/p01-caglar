//
//  AppDelegate.h
//  Hello, World!
//
//  Created by Kaan on 1/22/17.
//  Copyright © 2017 Kaan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

